#Librerias
import os
import sqlite3
from flask import Flask, render_template, request, session, escape
from flask import render_template, request, redirect, url_for, g
from flask_sqlalchemy import SQLAlchemy
from werkzeug.security import generate_password_hash, check_password_hash
from flask_mail import Mail, Message
from werkzeug import secure_filename
#Configuracion general del software
app = Flask(__name__)
#Configuracion del email
app.config.update(
    DEBUG = True,
    MAIL_SERVER = 'smtp.gmail.com',
    MAIL_PORT = 465,
    MAIL_USE_TLS = False,
    MAIL_USE_SSL = True,
    MAIL_USERNAME = 'pom.pile.of.money@gmail.com',
    MAIL_PASSWORD = 'pileofmoney2019',
    
)
mail = Mail(app)


app.config['DEBUG'] = True
app.config['SECRET_KEY'] = 'secret'
#Configuracion para update file
APP_ROOT = os.path.dirname(os.path.abspath(__file__))
app.config['UPLOAD_FOLDER'] = './Archivos PDF'


#Base de datos login de user
dbdir = "sqlite:///" + os.path.abspath(os.getcwd()) + "/database.db"
DATABASE = "data.db"
app.config["SQLALCHEMY_DATABASE_URI"] = dbdir
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db = SQLAlchemy(app)

class Users(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(50), unique=True, nullable=False)
    password = db.Column(db.String(80), nullable=False)

# Gestión de la base de datos proveedores
def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
        db.row_factory = sqlite3.Row
    return db

def query_db(query, args=(), one=False):
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    cur.close()
    return (rv[0] if rv else None) if one else rv

def change_db(query,args=()):
    cur = get_db().execute(query, args)
    get_db().commit()
    cur.close()

@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()


@app.route("/login", methods=["GET", "POST"])
def login():
    if request.method == "POST":
        user = Users.query.filter_by(username=request.form["username"]).first()

        if user and check_password_hash(user.password, request.form["password"]):
            session["username"] = user.username
            return render_template("redi.html")
        return render_template("loginerror.html")
    return render_template("login.html")

@app.route("/loginerror", methods=["GET", "POST"])
def loginerror():
    if request.method == "POST":
        user = Users.query.filter_by(username=request.form["username"]).first()

        if user and check_password_hash(user.password, request.form["password"]):
            session["username"] = user.username
            return render_template("redi.html")
        return render_template("loginerror.html")

    return render_template("login.html")

@app.route("/logout")
def logout():
    session.pop("username", None)
    return render_template("login.html")

@app.route("/subirbg")
def upload_file():
    if "username" in session:
        contact_list=query_db("SELECT * FROM contact")
 # renderiamos la plantilla "formulario.html"
        return render_template('formulario.html',contact_list=contact_list)
    return render_template("login.html")

@app.route("/upload", methods=['POST'])
def uploader():
    if "username" in session:
        contact_list=query_db("SELECT * FROM contact")
        if request.method == 'POST':
         # obtenemos el archivo del input "archivo"
            f = request.files['archivo']
            filename = secure_filename(f.filename)
        # Guardamos el archivo en el directorio "Archivos PDF"
            f.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        # Retornamos una respuesta satisfactoria
        return render_template('mensaje.html',contact_list=contact_list)
    return render_template("login.html")

@app.route("/pom")
def pom():
    if "username" in session:
        contact_list=query_db("SELECT * FROM contact")
        return render_template('POM.html',contact_list=contact_list)
    return render_template("login.html")

@app.route("/excel")
def excel():
    if "username" in session:
        contact_list=query_db("SELECT * FROM contact")
        return render_template('excel.html',contact_list=contact_list)
    return render_template("login.html")

@app.route("/reportes")
def reportes():
    if "username" in session:
        contact_list=query_db("SELECT * FROM contact")
        return render_template('reportes.html',contact_list=contact_list)
    return render_template("login.html")

@app.route("/mensaje")
def mensaje():
    if "username" in session:
        contact_list=query_db("SELECT * FROM contact")
        return render_template('mensaje.html',contact_list=contact_list)
    return render_template("login.html")

@app.route("/dashboard")
def dashboard():
    if "username" in session:
        contact_list=query_db("SELECT * FROM contact")
        return render_template('dashboard.html',contact_list=contact_list)
    return render_template("login.html")

@app.route('/nuevo', methods=['GET', 'POST'])
def nuevo():
    if "username" in session:
        contact_list=query_db("SELECT * FROM contact")
        if request.method == "GET":
            return render_template("formulario3.html",contact=None)
    
        if request.method == "POST":
            contact=request.form.to_dict()
            values=[contact['company'],contact['phone'],contact['mail'],contact['adress'],contact['city'],contact['country'],contact['website'],contact['industry'],contact['description'],contact['managername'],contact['manageremail'],contact['managerphone'],contact['finanzasname'],contact['finanzasemail'],contact['finanzasphone']]
            change_db("INSERT INTO contact (company,phone,mail,adress,city,country,website,industry,description,managername,manageremail,managerphone,finanzasname,finanzasemail,finanzasphone) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",values)
            return redirect(url_for("redifinal"),contact_list=contact_list)
    return render_template("login.html")

@app.route ('/')
def index():
    msg = Message('Nuevo proyecto registrado', sender = 'pom.pile.of.money@gmail.com', recipients = ['najerarodriguez04@gmail.com'])
    msg.body = "Saludos, se registro un nuevo proveedor. En la prueba no se adjunta la informacion del nuevo proveedor"
    mail.send(msg)
    return redirect(url_for("login"))

@app.route ('/redifinal')
def redifinal():
    if "username" in session:
        contact_list=query_db("SELECT * FROM contact")
        msg = Message('Nuevo proyecto registrado', sender = 'pom.pile.of.money@gmail.com', recipients = ['jpcordero@invermaster.com'])
        msg.body = "Saludos, se registro un nuevo proyecto."
        mail.send(msg)
        return render_template("redifinal.html",contact_list=contact_list)
    return render_template("login.html")



@app.route("/perfil")
def perfil():
    if "username" in session:
        contact_list=query_db("SELECT * FROM contact")
        return render_template('perfil.html',contact_list=contact_list)
    return render_template("login.html")

@app.route("/notificaciones")
def notificaciones():
    if "username" in session:
        contact_list=query_db("SELECT * FROM contact")
        return render_template('notificaciones.html',contact_list=contact_list)
    return render_template("login.html")

if __name__ == '__main__':
    app.run()